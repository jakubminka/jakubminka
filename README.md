# JAM_Stack
### Praktická část bakalářské práce na téma JAM Stack

Použité nástroje:
  * Statický generátor Jekyll + pluginy
  * Framework Bootstrap + Material Design
  * API: 
    - Algolia Search API
    - Netlify Forms API
    - Snipcart API
    - Disqus API
  * Repozitář GitHub.com
  * Headless CMS Netlify CMS
  * Hosting Netlify.com
  
Webová stránka je k dispozici na adrese https://zen-torvalds-384b19.netlify.com
